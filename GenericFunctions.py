# generic functions

import math
import random
import json


def get_information(species: str) -> dict:
    with open("species_data.json", "r") as read_dict:
        return json.load(read_dict)


def cartesian2polar(x: float, y: float):
    radius = math.sqrt(math.pow(x, 2) + math.pow(y, 2))
    if x > 0 and y >= 0:
        angle = math.atan(y / x)
    elif x > 0 and y < 0:
        angle = math.atan(y / x) + 2 * math.pi
    elif x < 0:
        angle = math.atan(y / x) + math.pi
    elif x == 0 and y > 0:
        angle = 0
    else:  # x == 0 and y < 0
        angle = 3 * math.pi / 2
    return radius, angle


def polar2cartesian(r: float, alpha: float):
    x = r * math.cos(alpha)
    y = r * math.sin(alpha)
    return x, y


def random_placement_tree(
    xcenter: float, ycentre: float, maxradius: float
):
    # within the circle
    r = random.randint(0, maxradius)
    alpha = math.radians(random.randint(0, 360))
    xcircle, ycircle = polar2cartesian(r, alpha)
    # placing the circle
    x = xcenter + xcircle
    y = ycentre + ycircle
    return x, y


def random_age(maxage: int) -> float:
    return random.randint(1, maxage)


def random_placement_seedling(xcentre: float, ycentre: float, maxradius: float):
    # within the circle around the tree
    r = random.gauss(0, maxradius / 2)
    alpha = math.radians(random.randint(0, 360))
    xcircle, ycircle = polar2cartesian(r, alpha)
    # placing the seeder
    x = xcentre + xcircle
    y = ycentre + ycircle
    return x, y