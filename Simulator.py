# forest simulator

import random
import math
import time

import GenericFunctions as gf
import Tree as Tree
import Forest as Forest


# main
# init
# create forest
tstart = time.time()
print("init...")
testforest = Forest.Forest("TestForest")

# create speciesA
testspeciesA = Tree.TreeSpecies("A")
# add species to forest
testforest.add_species(testspeciesA)
# try to add random trees to forest -> to species
for i in range(3):
    x, y = gf.random_placement_tree(0, 0, 20)
    testforest.add_tree(testspeciesA, 
        [x, y, gf.random_age(round(testspeciesA.endoflife / 2)), 0, 0, 0],
        Forest.OVERLAPPING_INIT
    )

# idem speciesB
testspeciesB = Tree.TreeSpecies("B")
testforest.add_species(testspeciesB)
for _ in range(2):
    x, y = gf.random_placement_tree(0, 0, 50)
    testforest.add_tree(testspeciesB,
        [x, y, gf.random_age(round(testspeciesB.endoflife / 2)), 0, 0, 0],
        Forest.OVERLAPPING_INIT
    )

#calculate g at init
testforest.gsum()

# display what is there
print(testspeciesA)
print(testspeciesB)
print(testforest)
testforest.graphforest()
input("-- press enter --")


# test over 50 years
for i in range(1, 51, 1):
    testforest.forest_time_step()
    print(testspeciesA)
    print(testspeciesB)
    print(testforest)
    if i%5==0:
        testforest.graphforest()

tend = time.time()
print("time =", tend - tstart, "s.")