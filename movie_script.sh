# using ffmpeg
# from https://stackoverflow.com/questions/24961127/how-to-create-a-video-from-images-with-ffmpeg
ffmpeg -framerate 3 -pattern_type glob -i 'Graphs/*.png' -c:v libx264  output.mp4
# framerate is img/sec


# using ImageMagick to make a gif
# from https://unix.stackexchange.com/questions/24014/creating-a-gif-animation-from-png-files#24019
convert -delay 50 -loop 0 Graphs/*.png Graphs/animation.gif
# with delay in tens of millisecs