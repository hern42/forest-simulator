# tree class

GENERAL_MORTALITY = 2  # in %

import math
import random
import numpy

import GenericFunctions as gf

def basal_area_calculation(diameter: float) -> float:
    return math.pow(diameter / 100, 2) * math.pi

def growth(age, enddiameter, lifeexpectancy):  # from a sigmoid model
    # a = yearly growth (end diameter / (middle life expectancy * 10))
    a = enddiameter / (((lifeexpectancy / 2) - 10) * 10)
    # b = end diameter
    b = enddiameter
    # c = middle of the central growth part = (life expectancy/2)
    c = (
        lifeexpectancy / 2
    ) - 10  # remove 10 to shift it towards the beginning to have growth at the beginning
    if age <= lifeexpectancy:
        diameter = b / (1 + math.exp(-a * (age - c)))
    else:
        diameter = enddiameter
    return diameter

def mortality(age, lifeexpectancy):
    return float(age > lifeexpectancy)

def random_mortality(moralitypercentage: int) -> bool:
    return random.random() > (1 - moralitypercentage / 100)


class TreeSpecies:
    def __init__(self, species):
        self.treelist = False # first tree is null to be able to add rows after
        dictionarytree = gf.get_information(species)
        self.speciesname = dictionarytree[species]["name"]
        self.enddiameter = dictionarytree[species]["enddiameter"]
        self.endoflife = dictionarytree[species]["endoflife"]
        self.seedingage = dictionarytree[species]["seedingage"]
        self.seedingrange = dictionarytree[species]["seedingrange"]
        self.seedingamount = dictionarytree[species]["seedingamount"]
        self.seedlingmortality = dictionarytree[species]["seedlingmortality"]
        self.mapcolour = dictionarytree[species]["mapcolour"]

    def __repr__(self):
        repr_string = ""
        for tree in self.treelist:
            x = tree[0]
            y = tree[1]
            radius, angle = gf.cartesian2polar(x, y)
            radius
            repr_string += "species: {} - age: {} - position: polar->({}, {})/cart.->({}, {}) - diameter: {} - dead?: {} - g: {}\n".format(
                self.speciesname,
                tree[2],
                round(radius),
                round(angle / math.pi * 180),
                round(x),
                round(y),
                round(tree[3], 1),
                bool(tree[4]),
                round(tree[5], 2)
            )
        return repr_string[:-1]

    def add_tree(self, xcenter, ycenter, age, dead, g):
        # one tree = [x, y, age, diameter, dead or not, basal area]
        diameter = growth(age, self.enddiameter, self.endoflife)
        g = basal_area_calculation(diameter)
        if isinstance(self.treelist, bool): # this means it's the first tree to be added
            self.treelist = numpy.array([xcenter, ycenter, age, diameter, dead, g])
        else:
            self.treelist = numpy.vstack([self.treelist, [xcenter, ycenter, age, diameter, dead, g]])

    def trees_time_step(self, increment):
        for tree in self.treelist:
            if not tree[4]:
                # growth
                tree[2] += increment
                tree[3] = growth(tree[2], self.enddiameter, self.endoflife)
                tree[4] = mortality(tree[2], self.endoflife)
                # random death
                if tree[2] > 3:
                    tree[4] = random_mortality(GENERAL_MORTALITY)
                # update of basal area
                tree[5] = basal_area_calculation(tree[3])
                # death of seedlings of year - 1
                if (tree[2] > 2) & (tree[2] <= 5):
                    tree[4] = random_mortality(self.seedlingmortality)
        # removal of dead seedlings from the list
        self.treelist = numpy.delete(
            self.treelist,
            numpy.where((self.treelist[:, 2] > 1) & (self.treelist[:, 2] <= 7) & (self.treelist[:, 4] == 1))[0], 
            axis = 0
            )


    def regeneration(self):
        self.seeding_list = False
        for tree in self.treelist:
            if tree[2] > self.seedingage:
                for i in range(self.seedingamount):
                    x, y = gf.random_placement_seedling(tree[0], tree[1], self.seedingrange)
                    if isinstance(self.seeding_list, bool):
                        self.seeding_list = numpy.array([x, y, 1, 0, 0, 0])
                    else:
                        self.seeding_list = numpy.vstack([self.seeding_list, [x, y, 1, 0, 0, 0]])