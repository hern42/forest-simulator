# forest class

OVERLAPPING_INIT = 5  # in metres
OVERLAPPING_INIT_SEEDLINGS = 0.5
TIME_INCREMENT = 1  # 1 year

import math
import numpy
import matplotlib.pyplot as plt


def check_overlapping(first, second, overlapmini):
    deltax = first[0] - second[0]
    deltay = first[1] - second[1]
    distance = math.sqrt(math.pow(deltax, 2) + math.pow(deltay, 2))
    return distance <= overlapmini


class Forest:
    def __init__(self, name):
        self.name = name
        self.treespecieslist = False
        self.g = []
        self.increment = 0

    def __repr__(self):
        treeamount = 0
        for species in self.treespecieslist:
            for tree in species.treelist:
                treeamount += 1

        return "age: {}, amount of trees: {} - g: {} m2".format(
            self.increment, treeamount, round(self.g[-1], 3)
        )

    def add_species(self, species):
        if isinstance(self.treespecieslist, bool):
            self.treespecieslist = [species]
        else:
            self.treespecieslist.append(species)

    def add_tree(self, species2add, tree2add, overlap):
        if isinstance(species2add.treelist, bool): # this means it's the first tree to be added, no distance check needed
            species2add.add_tree(tree2add[0], tree2add[1], tree2add[2], 0, 0)
        else:
            # create an array with same size as treelist containing tree2add on each line
            array2test = numpy.full((len(species2add.treelist), 6), tree2add)
            # do the difference of x and y (we do the whole array in one go)
            array3 = species2add.treelist - array2test
            # calculate pythagoras
            array4 = numpy.sqrt(array3[:, 0] ** 2 + array3[:, 1] ** 2)
            # get the smallest difference of distance
            radius = min(array4)
            # check if the distance is smaller than the allowed overlaping unit
            if radius > overlap:
                species2add.add_tree(tree2add[0], tree2add[1], tree2add[2], 0, 0)

    def gsum(self):
        gvar = 0
        for species in self.treespecieslist:
            array = numpy.delete(
                species.treelist,
                numpy.where((species.treelist[:, 4] == 1) & (species.treelist[:, 3] < 17.5))[0],
                axis = 0
            )
            gvar += numpy.sum(species.treelist[:, 5], axis = 0)
        self.g.append(gvar)

    def forest_time_step(self):
        self.increment += TIME_INCREMENT
        for species in self.treespecieslist:
            # growth of trees for each species
            species.trees_time_step(TIME_INCREMENT)
            # add regeneration for each species
            species.regeneration()
            if not isinstance(species.seeding_list, bool):
                for seedling in species.seeding_list:
                    self.add_tree(species, seedling, OVERLAPPING_INIT_SEEDLINGS)
            else:
                pass
        self.gsum()

    def graphforest(self):
        print("graphing!!")
        fig, graph = plt.subplot_mosaic(
            [["main", "growthmodel"], ["main", "g"]],
            figsize=(15, 10),
            gridspec_kw={"width_ratios": [2, 1]},
            constrained_layout=True,
        )
        title = "Representation of a forest at age " + str(self.increment)
        fig.suptitle(title, fontsize=12)

        # main "map"
        graph["main"].axhline(y=0, color="black", ls="-", lw=1, marker="")
        graph["main"].axvline(x=0, color="black", ls="-", lw=1, marker="")

        for i in range(1, 6):
            graph["main"].add_artist(
                plt.Circle(
                    (0, 0), 10 * i, color="black", ls="--", alpha=0.25, fill=False
                )
            )

        for species in self.treespecieslist:
            for tree in species.treelist:
                if not tree[4]:
                    treecolour = species.mapcolour
                    circlesize = tree[3] / (2 * 100)
                    graph["main"].add_artist(
                        plt.Circle(
                            (tree[0], tree[1]),
                            circlesize * 10,
                            color=treecolour,
                            ls="",
                            alpha=1,
                            fill=True,
                        )
                    )
                    # graph["main"].scatter(
                    #     tree[0], tree[1], color="black", marker="."
                    # )  # marker for center of the tree

                elif tree[2] >= 5:
                    treecolour = "black"
                    markersize = 20
                    markertype = "+"
                    graph["main"].scatter(
                        tree[0],
                        tree[1],
                        color=treecolour,
                        s=markersize,
                        alpha=0.75,
                        marker=markertype,
                    )
                else:
                    # for debug
                    # treecolour = "black"
                    # markersize = 10
                    # markertype = "."
                    # graph["main"].scatter(
                    #     tree[0],
                    #     tree[1],
                    #     color=treecolour,
                    #     s=markersize,
                    #     alpha=0.75,
                    #     marker=markertype,
                    # )
                    pass


        graph["main"].set_title("map-y top/down view (diameters to scale) ")
        graph["main"].grid(True, color="black", ls="--", alpha=0.25)
        graph["main"].set_xlabel("x")
        graph["main"].set_xlim(-50, 50)
        graph["main"].set_xticks(numpy.arange(-50, 50 + 1, 5))
        graph["main"].set_ylabel("y")
        graph["main"].set_ylim(-50, 50)
        graph["main"].set_yticks(numpy.arange(-50, 50 + 1, 5))

        # growth models
        graph["growthmodel"].set_title("growth model")
        graph["growthmodel"].grid(True, color="black", ls="--", alpha=0.25)
        graph["growthmodel"].set_xlim(0, 100)
        graph["growthmodel"].set_xlabel("age")
        graph["growthmodel"].set_ylim(0, 100)
        graph["growthmodel"].set_ylabel("diameter")

        # g = f(t)
        for i in range(self.increment + 1):
            graph["g"].scatter(
                x=i, y=self.g[i], s=20, color="green", marker="d", alpha=1
            )

        graph["g"].set_title("basal area / ha over time")
        graph["g"].grid(True, color="black", ls="--", alpha=0.25)
        graph["g"].set_xlim(0, 100)
        graph["g"].set_xlabel("step")
        graph["g"].set_ylim(0, self.g[-1] + 10)
        graph["g"].set_ylabel("g (m2/ha)")

        plt.savefig(
            "Graphs/forest_" + "{0:0{width}}".format(self.increment, width=3) + ".png"
        )
        plt.close()
